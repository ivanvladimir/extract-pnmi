
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/)

# Extract PNMI

Code to create extract DB from [Padrón Nacional de Medios Impresos](https://pnmi.segob.gob.mx/)

## Dependencies

* click==8.1.3
* click-option-group==0.5.5
* parsel==1.7.0
* requests==2.28.1
* rich-click==1.6.0
* tinydb==4.7.0

## Install

To install:


    python -m venv env
    source env/bin/activate
    pip install -r requirements


## Running

### Extracting newspaper information

Extract the newspaper information from the oficial database
    
    python extract_pnmi.py extract

The information for default is in: `newspapers.tinydb.json`


### Listing media

List the newspaper in json format
    
    python extract_pnmi.py list


### Populate twitter 

Populates the db with the _twitter_ account 
    
    python extract_pnmi.py extract-twitter


### Create _csv_ 

Exports a _cvs_ wiht main information 
    
    python extract_pnmi.py export-csv

The csv by default is `output.csv`

## Authors

- [@ivanvladimir](https://www.gitlab.com/ivanvladimir)

