#p!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Ivan Vladimir Meza Ruiz 2022
# GPL 3.0


from click_option_group import optgroup
from rich import print
from rich.progress import track
import rich_click as click
import sys
import configparser
import os.path
import requests
from requests.exceptions import ConnectTimeout, ReadTimeout
import parsel
import re
import csv
import random
import time
from datetime import datetime, timezone
from copy import deepcopy
from tinydb import TinyDB, Query
from collections import Counter

DEFAULT_SECTION="DEFAULT"
HEADER={
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "en-GB,es-MX;q=0.7,en;q=0.3",
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "cross-site",
        "Sec-Fetch-User": "?1",
        "Sec-GPC": "1"
    }

def filter_url(url):
    bits=url.split("/")
    if len(bits)>4:
        return None
    bits=url.split("?",1)
    t=bits[0].lower()
    t=t.replace("www.","").replace("@","")
    if t in ["https://twitter.com/themerexthemes"]:
        return None
    return t

def get_config(ctx,args):
    try:
        config = dict(deepcopy(ctx.obj['config'][ctx.obj["config_section"]]))
    except NoneType:
        config = {}
    for k,v in config.items():
        if v:
            if k in ['sleep_seconds','max']:
                config[k]=int(v)
            elif k in ['insert_only_new','random']:
                config[k]=bool(v)
    for k,v in args.items():
        if v:
            if k in ['sleep_seconds','max']:
                config[k]=int(v)
            elif k in ['insert_only_new','random']:
                config[k]=bool(v)
            else:
                config[k]=v
    return config

@click.group()
@click.option("--config-filename", type=click.Path(), default="config.ini")
@click.option("--config-section", type=str, default=DEFAULT_SECTION)
@click.option("-v", "--verbose", is_flag=True, help="Verbose mode")
@click.pass_context
def extract_pnmi(
    ctx, config_filename, config_section,verbose=False
):
    ctx.ensure_object(dict)

    if os.path.exists(config_filename):
        config = configparser.ConfigParser(
                defaults={
                    "database_filename":'newspaper.tinydb.json',
                    "website_url":'https://pnmi.segob.gob.mx/',
                    "sleep_seconds":5,
                    "insert_only_new":True,
                    "max":0,
                    "random":True,
                    "output":"output.csv"}
                )
        config.read(config_filename)
    else:
        config={DEFAULT_SECTION:{}}
        print(f"[yellow]Warning '{config_filename}' not found found; using empty config[/] [white bold]: {config}[/]")
    if not config_section in config:
        print(f"[red]Error '{config_section}' section not found[/]")
        sys.exit(100)

    ctx.obj["config"] = config
    ctx.obj["config_section"] = config_section
    ctx.obj["verbose"] = verbose

@extract_pnmi.command()
@click.option("--database-filename", type=str, help="Database filename")
@click.pass_context
def list(
    ctx,
    **args,
):
    """Print newspapers"""
    config=get_config(ctx,args)
    db=TinyDB(config['database_filename'])
    for r in db:
        print(r)
    print(f'[green]Total: {len(db)}[/]')


@extract_pnmi.command()
@click.option("--database-filename", type=str, help="Database filename")
@click.option("--output", type=str, help="Output filename")
@click.pass_context
def export_csv(
    ctx,
    **args,
):
    """Export newspapers"""
    config=get_config(ctx,args)
    db=TinyDB(config['database_filename'])
    fields=['id','nombre','expediente','estado','municipio','url','twitter']
    rows=[]
    for r in db:
        row=[
                r['id_tramite'],
                r['name'],
                r['No. Expediente en CCPRI'],
                r['Información inicial']['Estado'],
                r['Información inicial']['Municipio'],
                r['Directorio']['Página electrónica'] if 'Página electrónica' in r['Directorio'] else 'None',
                ]
        if 'twitter' in r:
            row.extend([v for _,v in r['twitter'].items()])
        rows.append(row)

    with open(config['output'], 'w') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(fields)
        csvwriter.writerows(rows)


re_twitter_mention=re.compile(r"(https://(?:www\.)?twitter\.com/[^'\"]+)")
re_twitter_content=re.compile(r"meta\s+name=.twitter:site.\s+content=.([^'\"]+).")

@extract_pnmi.command()
@click.option("--database-filename", type=str, help="Database filename")
@click.option("--output", type=str, help="Output filename")
@click.option("--max",type=int,help="Maximum number of sources to analyse")
@click.pass_context
def extract_twitter(
    ctx,
    **args,
):
    """Export newspapers"""
    NP=Query()
    config=get_config(ctx,args)
    db=TinyDB(config['database_filename'])
    headers=dict(HEADER)
    headers.update({})
    stats=Counter()
    for i,r in enumerate(track(db)):
        if config['max']>0 and i>= config['max']:
            break
        stats['sources']+=1
        url=r['Directorio']['Página electrónica'] if 'Página electrónica' in r['Directorio'] else 'None'
        ts=set()
        if url and not url.lower().startswith("no"):
            url=f"https://{url}" if not url.startswith('http') else url
            print(url)
            try:
                r_=requests.get(f'{url}',
                    timeout=20,
                    headers=headers,
                    )
            except ReadTimeout:
                print(f"[yellow]Read time out: {r['id_tramite']}[/]")
                stats["timeout"]+=1
                continue
            except ConnectTimeout:
                print(f"[yellow]Connection time out: {r['id_tramite']}[/]")
                stats["timeout"]+=1
                continue
            except:
                print(f"[yellow]Connection time out: {r['id_tramite']}[/]")
                stats["timeout"]+=1
                continue
            # Twitter
            # head - meta
            text=r_.text
            for m in re_twitter_content.finditer(text):
                ts.add(filter_url(f"https://twitter.com/{m.group(1)}"))
            for m in re_twitter_mention.finditer(text):
                ts.add(filter_url(m.group(1)))
            if len(ts):
                stats['with_twitter']+=1
                stats['urls']+=len([t for t in ts if t])
        if len(ts):
            dt=datetime.now(timezone.utc)
            db.update({
                'date_updated':str(dt),
                'twitter':{f"{i}":v for i,v in enumerate(ts) if v }},NP.id_tramite==r['id_tramite'])
    print(f"[green]Sources            :[bold]{stats['sources']: 5d}[/][/]:")
    print(f"[green]Timeout            :[bold]{stats['timeout']: 5d}[/][/]:")
    print(f"[green]Sites with twitter :[bold]{stats['with_twitter']: 5d}[/][/]:")
    print(f"[green]Total urls         :[bold]{stats['urls']: 5d}[/][/]:")




@extract_pnmi.command()
@click.option("--database-filename", type=str, help="Database filename")
@click.option("--website-url", type=str, help="PNMI website url")
@click.option("--sleep_seconds", type=int, help="Seconds to await between request")
@click.option("--insert_only_new",type=bool,help="Insert only new newspaper entries")
@click.option("--random",type=bool,help="Request sources randomly")
@click.option("--max",type=int,help="Maximum number of sources to analyse")
@click.pass_context
def extract(
    ctx,
    **args,
):
    """Update newspapers from website"""
    config=get_config(ctx,args)
    db=TinyDB(config['database_filename'])
    NP=Query()
    print(f"[blue]Extracting info from: [bold]{config['website_url']}[/][/]")
    headers=dict(HEADER)
    headers.update({
        'Referer': 'https://pnmi.segob.gob.mx/reporte',
        'Origin': 'https://pnmi.segob.gob.mx',
    })

    r=requests.post(f'{config["website_url"]}reporte/resultado',headers=headers)
    sel=parsel.Selector(text=r.text)

    sources=sel.xpath('//div[has-class("col-md-10","resultado")]/@data-href').getall()

    print("Total sources identified",len(sources))
    if config['random']:
        random.shuffle(sources)
    stats=Counter()
    sources = sources[:config['max']] if config['max'] else sources
    for href in track(sources):
        stats['sources']+=1
        m=re.search(r'id=(\d+)',href)
        raw={'id':f'{m.group(1)}'}
        for t in range(10):
            try:
                r=requests.post(
                    f'{config["website_url"]}reporte/tramite',
                    timeout=5,
                    headers=headers,
                    data=raw)
                break
            except ReadTimeout:
                print(f"[yellow]Read time out: {raw['id']} (try:{t})[/]")
                time.sleep(config['sleep_seconds'])

            except ConnectTimeout:
                print(f"[yellow]Connection time out: {raw['id']} (try:{t})[/]")
                time.sleep(config['sleep_seconds'])
        newspaper={'requests':{
            'data':raw,
            'url':f'{config["website_url"]}reporte/tramite',
            'headers':headers,
            },
            'id_tramite':raw['id']}
        sel=parsel.Selector(text=r.text)
        sel.xpath("/html/body/main/div/div[3]/div/div[1]/div[2]/div[2]/div/p/img").drop()
        newspaper['name']=sel.xpath('//h3/text()').get().strip()
        row=sel.xpath('/html/body/main/div/div[2]')
        for p in row.xpath('.//p'):
            t="".join(p.root.itertext())
            bits=t.split(':',1)
            k=bits[0].strip()
            v=bits[1].strip()
            newspaper[k]=v
        for i,tab in enumerate(sel.xpath('//div[has-class("tab-pane")]')):
            secs=tab.xpath('.//h5/text()')
            cons=tab.xpath('.//div[@class="row"]')
            if i == 1:
                secs=secs[0:5]+4*secs[5:6]+secs[6:]
            for sec,con in zip(secs,cons):
                sec=sec.get()
                new_sec={}
                for p in con.xpath('.//p'):
                    t_="".join(p.root.itertext())
                    t_=[t_]
                    if 'Circulación y distribución geográfica' in sec:
                        if t_[0].startswith('Inicio'):
                            t_=t_[0].split('\t',1)
                    if 'Medida del impreso' in sec:
                        if t_[0].startswith('Ancho'):
                            t_=t_[0].split(u'\xa0',1)
                    for t in t_:
                        if len(t.strip())==0:
                            continue
                        bits=t.split(':',1)
                        if len(bits)==1:
                            continue
                        k=bits[0].strip()
                        v=bits[1].strip()
                        new_sec[k]=v
                if len(new_sec)>0:
                    if not sec.strip() in newspaper:
                        newspaper[sec.strip()]=new_sec
                    else:
                        newspaper[sec.strip()].update(new_sec)

        dt=datetime.now(timezone.utc)
        if len(db.search(NP.id_tramite==raw['id']))==0:
            stats['inserted']+=1
            newspaper['date_created']=newspaper['date_updated']=str(dt)
            db.insert(newspaper)
        else:
            if config['insert_only_new']:
                stats['ignored']+=1
            else:
                stats['updated']+=1
                newspaper['date_updated']=str(dt)
                db.update(newspaper,NP.id_tramite==raw['id'])
        time.sleep(config['sleep_seconds'])
    print(f"[green]Sources   :[bold]{stats['sources']: 5d}[/][/]:")
    print(f"[green]Updated   :[bold]{stats['updated']: 5d}[/][/]:")
    print(f"[green]Inserted  :[bold]{stats['inserted']: 5d}[/][/]:")
    print(f"[green]Ignored   :[bold]{stats['ignored']: 5d}[/][/]:")


if __name__ == "__main__":
    extract_pnmi(obj={})
